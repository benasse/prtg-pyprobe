---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Sensor for monitoring the total amount of your S3 buckets.

---
#### Configuration
**Timeout**: If the reply takes longer than this value the request is aborted and an error message is triggered.  
**Script Name**:  Enter the name of the script located in /var/prtg/scripts (you might have to mount the directory when running the pyprobe container).  
**Script Arguments**:  Enter the arguments which should be passed to your script.

**WARNING**
Although the path is checked for path traversals, this is NOT done for script arguments.

---
![Script](../img/sensor_script.png){: align=left }

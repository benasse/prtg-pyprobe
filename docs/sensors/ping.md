---
**WARNING**  
To use the ICMP mode of this sensor, the pyprobe must run as root as ICMP requests require access to raw sockets.

---
#### Description
Monitors the availability of a target using TCP or ICMP.

---
#### Configuration
**Timeout**: If the reply takes longer than this value the request is aborted and an error message is triggered.  
**Ping Count**:  Enter the count of Ping requests PRTG will send to the device during an interval.  
**Port (TCP mode only)**:  This is the port that you want to monitor if it's open or closed. Only used for TCP Ping.  
**Ping Type**: Choose your Ping type. Note that for ICMP the pyprobe must run as root!  

---
![Ping](../img/sensor_ping.png){: align=left }

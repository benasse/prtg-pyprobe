import asyncio
import collections
import platform

import psutil
import pytest

import prtg_pyprobe.sensors.sensor_probe_temperature as module
from prtg_pyprobe.sensors.sensor import SensorPSUtilBase


def task_data(diskfull="1"):
    return {
        "sensorid": "1234",
        "diskfull": diskfull,
        "host": "127.0.0.1",
        "kind": "mpprobehealth",
        "celfar": "1",
    }


def uname_res():
    uname_result_mock = collections.namedtuple("uname_result", "system node release version machine processor")
    return uname_result_mock(
        system="Darwin",
        node="NUE-MAC-065",
        release="19.5.0",
        version="Darwin Kernel Version 19.5.0: Tue May 26 20:41:44 PDT 2020; root:xnu-6153.121.2~2/RELEASE_X86_64",
        machine="x86_64",
        processor="i386",
    )


def psutil_error():
    raise psutil.Error


class TestProbeTempSensorProperties:
    def test_sensor_probe_temp_base_class(self, probetemp_sensor):
        assert type(probetemp_sensor).__bases__[0] is SensorPSUtilBase

    def test_sensor_probe_temp_name(self, probetemp_sensor):
        assert probetemp_sensor.name == "Temperature (Probe)"

    def test_sensor_probe_temp_kind(self, probetemp_sensor):
        assert probetemp_sensor.kind == "mpprobetemperature"

    def test_sensor_probe_temp_definition(self, probetemp_sensor):
        assert probetemp_sensor.definition == {
            "description": "Sensor used to monitor the temperature of the Python probe.",
            "groups": [
                {
                    "caption": "Temperature Settings",
                    "fields": [
                        {
                            "caption": "Choose between Celsius or Fahrenheit " "display",
                            "default": "1",
                            "help": "Choose whether you want to return the value "
                            "in Celsius or Fahrenheit. When changed after "
                            "sensor is created, the unit will disappear "
                            "in the web interface.",
                            "name": "celfar",
                            "options": {"1": "Celsius", "2": "Fahrenheit"},
                            "type": "radio",
                        }
                    ],
                    "name": "temperature",
                }
            ],
            "help": "This sensor monitors the temp of the probe itself. This is always "
            "the case even if added to a different device than the Probe Device.",
            "kind": "mpprobetemperature",
            "name": "Temperature (Probe)",
            "tag": "mpprobetemperaturesensor",
        }


@pytest.mark.asyncio
class TestProbeTemperatureSensorWork:
    async def test_sensor_probe_temp(self, probetemp_sensor, monkeypatch, mocker, psutil_system_temperatures):
        monkeypatch.setattr(platform, "uname", uname_res)
        psutil_mock = mocker.patch.object(probetemp_sensor, "get_system_temperatures")
        psutil_mock.return_value = psutil_system_temperatures
        probetemp_queue = asyncio.Queue()
        await probetemp_sensor.work(task_data=task_data(), q=probetemp_queue)
        out = await probetemp_queue.get()
        assert out == {
            "channel": [
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "acpitz",
                    "unit": "Custom",
                    "value": 60.5,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Package id 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 1",
                    "unit": "Custom",
                    "value": 52.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Package id 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 1",
                    "unit": "Custom",
                    "value": 52.0,
                },
                {"customunit": "C", "kind": "Custom", "mode": "float", "name": "CPU", "unit": "Custom", "value": 60.0},
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Ambient",
                    "unit": "Custom",
                    "value": 39.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Ambient",
                    "unit": "Custom",
                    "value": 32.0,
                },
                {"customunit": "C", "kind": "Custom", "mode": "float", "name": "GPU", "unit": "Custom", "value": 16.0},
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Other",
                    "unit": "Custom",
                    "value": 43.0,
                },
            ],
            "message": "Temperatures on Darwin, everything OK",
            "sensorid": "1234",
        }

    async def test_sensor_probe_temp_no_sensors(self, probetemp_sensor, monkeypatch, sensor_exception_message, mocker):
        monkeypatch.setattr(platform, "uname", uname_res)
        psutil_mock = mocker.patch.object(probetemp_sensor, "get_system_temperatures")
        psutil_mock.return_value = {}
        logging_mock = mocker.patch.object(module, "logging")

        probetemp_queue = asyncio.Queue()
        await probetemp_sensor.work(task_data=task_data(), q=probetemp_queue)
        out = await probetemp_queue.get()
        sensor_exception_message["message"] = (
            "The sensor could not find temperature sensors on your probe " "machine. See log for details"
        )
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_once_with("No temperature sensors could be found by PSUtil")

    async def test_sensor_probe_temp_psutil_exception(
        self, probetemp_sensor, monkeypatch, sensor_exception_message, mocker
    ):
        logging_mock = mocker.patch.object(module, "logging")
        mocker.patch.object(probetemp_sensor, "get_system_temperatures", side_effect=psutil.Error())

        probehealth_queue = asyncio.Queue()
        await probetemp_sensor.work(task_data=task_data(), q=probehealth_queue)
        out = await probehealth_queue.get()
        sensor_exception_message["message"] = "Probe temperature check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_once_with("Probe temperature could not be determined")

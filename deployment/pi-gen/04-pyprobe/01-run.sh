#!/bin/bash -e
on_chroot << EOF
  cd /home/pi
  git clone https://gitlab.com/paessler-labs/prtg-pyprobe
  python3 -m venv /opt/piprobe-venv
  echo export PATH=\"/opt/piprobe-venv/bin:$PATH\" >> /home/pi/.bashrc
  echo export PATH=\"/opt/piprobe-venv/bin:$PATH\" >> /root/.bashrc
  cd pyprobe
  /opt/piprobe-venv/bin/python -m pip install wheel
  /opt/piprobe-venv/bin/python -m pip install .
EOF

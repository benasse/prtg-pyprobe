---
#### Description
Sensor for monitoring the total amount of your S3 buckets.

---
#### Configuration
**AWS Access Key ID**: AWS Access Key ID of the account you want to monitor.  
**AWS Secret Key**:  AWS Secret Key of the account you want to monitor.

---
![S3 Bucket Total](../img/sensor_s3_bucket_total.png){: align=left }

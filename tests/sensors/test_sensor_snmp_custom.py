import asyncio

import pytest
from pysnmp.error import PySnmpError
from pysnmp.hlapi.asyncio import SnmpEngine
from pysnmp.smi.error import NoSuchObjectError

from prtg_pyprobe.sensors import sensor_snmp_custom as module
from prtg_pyprobe.sensors.sensor import SensorSNMPBase


async def snmp_get_success(*args, **kwargs):
    return [("1.3.6.1.1.2.3.4", 1234)]


async def snmp_get_empty_value(*args, **kwargs):
    return [("1.3.6.1.1.2.3.4", "")]


async def snmp_get_pysnmp_error(*args, **kwargs):
    raise PySnmpError


async def snmp_get_runtime_error(*args, **kwargs):
    raise RuntimeError


async def snmp_get_value_error(*args, **kwargs):
    raise ValueError


async def snmp_get_no_such_object_error(*args, **kwargs):
    raise NoSuchObjectError


class TestSNMPCustomSensorProperties:
    def test_sensor_snmp_custom_base_class(self, snmp_custom_sensor):
        assert type(snmp_custom_sensor).__bases__[0] is SensorSNMPBase

    def test_sensor_snmp_custom_name(self, snmp_custom_sensor):
        assert snmp_custom_sensor.name == "SNMP Custom"

    def test_sensor_snmp_custom_kind(self, snmp_custom_sensor):
        assert snmp_custom_sensor.kind == "mpsnmpcustom"

    def test_sensor_snmp_custom_definition(self, snmp_custom_sensor):
        assert snmp_custom_sensor.definition == {
            "description": "Monitors a numerical value returned by a specific OID using " "SNMP",
            "groups": [
                {
                    "caption": "Sensor Specific",
                    "fields": [
                        {
                            "caption": "Custom OID",
                            "help": "Provide an OID which is returning a numeric " "value",
                            "name": "custom_oid",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "Custom Unit",
                            "help": "Provide an unit to be shown with the value.",
                            "name": "custom_unit",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "Value Type",
                            "default": "integer",
                            "help": "Select 'Gauge' if you want to see absolute "
                            "values (e.g. for temperature value) or "
                            "'Delta' for counter differences divided by "
                            "time period (e.g. for bandwidth values)",
                            "name": "value_type",
                            "options": {"counter": "Delta", "integer": "Gauge"},
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "sensor_specific",
                },
                {
                    "caption": "SNMP Specific",
                    "fields": [
                        {
                            "caption": "Community String",
                            "help": "Please enter the community string.",
                            "name": "community",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "SNMP Port",
                            "help": "Please enter SNMP Port.",
                            "name": "port",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "SNMP Version",
                            "default": "1",
                            "help": "Choose your SNMP Version",
                            "name": "snmp_version",
                            "options": {"0": "V1", "1": "V2c"},
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "snmpspecific",
                },
            ],
            "help": "Monitors a numerical value returned by a specific OID using SNMP",
            "kind": "mpsnmpcustom",
            "name": "SNMP Custom",
            "tag": "mpsnmpcustomsensor",
        }


@pytest.mark.asyncio
class TestSNMPCustomSensorWork:
    async def test_sensor_snmp_custom_work_success(self, snmp_custom_sensor, snmp_custom_sensor_taskdata, monkeypatch):
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_success)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_custom_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        assert ret["sensorid"] == "1234"
        assert ret["message"] == "OK"
        assert {
            "name": "1.3.6.1.1.2.3.4",
            "mode": "integer",
            "value": 1234,
            "unit": "Custom",
            "customunit": "test",
        } in ret["channel"]

    async def test_sensor_snmp_custom_work_empty_value(
        self,
        mocker,
        snmp_custom_sensor,
        snmp_custom_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_empty_value)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_custom_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error in PySNMP.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_custom_work_pysnmp_error(
        self,
        mocker,
        snmp_custom_sensor,
        snmp_custom_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_pysnmp_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_custom_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error in PySNMP.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_custom_work_runtime_error(
        self,
        mocker,
        snmp_custom_sensor,
        snmp_custom_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_runtime_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_custom_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_custom_work_value_error(
        self,
        mocker,
        snmp_custom_sensor,
        snmp_custom_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_value_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_custom_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "The SNMP Query did return a not supported value type."
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("The SNMP query probably returned an unsupported value.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_custom_no_such_object_error(
        self,
        mocker,
        snmp_custom_sensor,
        snmp_custom_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_no_such_object_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_custom_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "The OID is wrong."
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("No such object. Probably the given OID is wrong.")
        logging_mock.exception.assert_called_once()

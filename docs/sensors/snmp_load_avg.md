---
#### Description
This sensor monitors the load avg of a given target via SNMP.

---
#### Configuration
**Community String**:  Enter the community string.  
**SNMP Port**:  Enter the SNMP Port.  
**SNMP Version**: Choose your SNMP Version.  

---
![SNMP CPU Load Avg](../img/sensor_snmp_load_avg.png){: align=left }
